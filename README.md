# FastAPI Library System

A simple library management system implemented using FastAPI to manage books.

## Description

This FastAPI application allows you to add, retrieve, update, and delete books in a library system. Each book is tracked by a serial number and can be borrowed or returned by updating its status.

## Features

- **Add Books:** Add books to the system.
- **Delete Books:** Remove books from the system.
- **Get All Books:** Retrieve all books currently stored in the system.
- **Update Book Status:** Update the borrow status of the books.

Each book entry includes:
- Serial Number
- Title
- Author
- Borrow Status (whether the book is borrowed)
- Borrowed By (ID of the borrower, must be a 6-digit integer)
- Borrowed On (Date when the book was borrowed, formatted as 'YYYYMMDD')

## Installation

1. Clone the repository:
    ```bash
    git clone [repository-url]
    cd [repository-name]
    ```
2. Install the required dependencies:
    ```bash
    pip install -r requirements.txt
    ```

## Usage

To run the application:

```bash
uvicorn main:app --reload
```

This will start the FastAPI server on the default host and port, typically `127.0.0.1:8000`.

Run the application using docker:
```bash
docker compose up --build
```
## Endpoints
 -   `POST /books/`: Add a new book.
 -   `DELETE /books/{serial_number}`: Delete a book by its serial number.
 -   `GET /books/`: Get a list of all books.
 -   `PUT /books/{serial_number}`: Update the borrow status of a book.
 -   `DOCS /docs#/`: Interactive documentation. Contains detailed information about all endpoints.

## Curl Commands

### Add a Book
```bash
curl -X POST "http://127.0.0.1:8000/books/" \
-H "Content-Type: application/json" \
-d '{
    "serial_number": 123456,
    "title": "The Great Gatsby",
    "author": "F. Scott Fitzgerald",
    "is_borrowed": false,
    "borrowed_by": null,
    "borrowed_on": null
}'
```

### Get All Books
```bash
curl -X GET "http://127.0.0.1:8000/books/"
```

### Delete a Book
```bash
curl -X DELETE "http://127.0.0.1:8000/books/123456"
```

### Update Book Borrow Status
```bash
curl -X PUT "http://127.0.0.1:8000/books/123456" \
-H "Content-Type: application/json" \
-d '{
    "is_borrowed": true,
    "borrower_id": 654321,
    "borrowed_on": "20240620"
}'
```
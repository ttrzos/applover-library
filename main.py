from fastapi import FastAPI, HTTPException
from pydantic import BaseModel, validator
from typing import Optional, List
from datetime import datetime, date

app = FastAPI()
books_db = {}


class Book(BaseModel):
    serial_number: int
    title: str
    author: str
    is_borrowed: bool = False
    borrowed_by: Optional[int] = None
    borrowed_on: Optional[date | str] = None

    @validator('serial_number', 'borrowed_by', always=True, pre=True)
    def validate_six_digit_id(cls, v):
        if v is not None and len(str(v)) != 6:
            raise ValueError("ID must be a 6 digit integer.")
        return v

    @validator('borrowed_on', pre=True)
    def parse_date(cls, v):
        if v is not None:
            try:
                return datetime.strptime(v, "%Y-%m-%d").date()
            except Exception:
                raise ValueError("Date must be a string in YYYY-MM-DD format.")
        return v


def check_borrow_details(is_borrowed: bool, borrowed_by: Optional[int], borrowed_on: Optional[str]):
    if is_borrowed:
        if borrowed_by is None or borrowed_on is None:
            raise HTTPException(status_code=400, detail="Both borrowed_by and borrowed_on parameters must be provided.")
    else:
        borrowed_by, borrowed_on = None, None
    return borrowed_by, borrowed_on


@app.post("/books/", response_model=List[Book])
async def add_book(book: Book) -> List[Book]:
    if book.serial_number in books_db:
        raise HTTPException(status_code=400, detail="Book already exists.")
    book.borrowed_by, book.borrowed_on = check_borrow_details(book.is_borrowed, book.borrowed_by, book.borrowed_on)
    books_db[book.serial_number] = book
    return list(books_db.values())


@app.delete("/books/{serial_number}", status_code=200, response_model=List[Book])
async def delete_book(serial_number: int) -> List[Book]:
    if serial_number not in books_db:
        raise HTTPException(status_code=404, detail="Book not found.")
    del books_db[serial_number]
    return list(books_db.values())


@app.get("/books/", status_code=200, response_model=List[Book])
async def get_all_books() -> List[Book]:
    return list(books_db.values())


@app.put("/books/{serial_number}", status_code=200, response_model=Book)
async def update_book_status(serial_number: int, is_borrowed: bool, borrower_id: Optional[int] = None,
                       borrowed_on: Optional[str] = None) -> Book:

    try:
        serial_number = Book.validate_six_digit_id(serial_number)
        borrower_id = Book.validate_six_digit_id(borrower_id)
        borrowed_on = Book.parse_date(borrowed_on)
    except Exception as e:
        raise HTTPException(status_code=400, detail=str(e))

    if serial_number not in books_db:
        raise HTTPException(status_code=404, detail="Book not found.")

    book = books_db[serial_number]
    book.borrowed_by, book.borrowed_on = check_borrow_details(is_borrowed, borrower_id, borrowed_on)
    book.is_borrowed = is_borrowed
    return book
